@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Профил</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/edit_profile/update') }}">
                        {!! csrf_field() !!}

                        <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                            @foreach($entries as $entry)
                                <label class="col-md-4 control-label">Първо име</label>
                                <input type="text" class="form-control" name="first_name" value="{{$entry->first_name}}" >

                                <label class="col-md-4 control-label">Фамилия</label>
                                <input type="text" class="form-control" name="last_name" value="{{$entry->last_name}}" >

                                <label class="col-md-4 control-label">E-mail адрес</label>
                                <input type="text" class="form-control" name="email" value="{{$entry->email}}" >

                                <label class="col-md-4 control-label">Вие сте:</label>
                                <select name="user_role" class="form-control user_role-select">
                                    <option value="">Избери опция...</option>
                                    <option value="teacher">Учител</option>
                                    <option value="parent">Родител</option>
                                    <option value="student">Ученик</option>
                                </select>


                            @endforeach
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i>Обнови
                                </button>
                            </div>
                        </div>
                    </form>



                </div>
            </div>
        </div>
    </div>
</div>
@endsection