@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Учебна програма</div>
                <div class="panel-body">
                        {!! csrf_field() !!}
                        <style>
                            table, th, td {
                                border: 1px solid black;
                            }
                        </style>

                    <table>
                          <tr>
                            <th>Понеделник</th>
                            <td>БЕЛ</td>
                            <td>БЕЛ</td>
                            <td>Математика</td>
                            <td>Математика</td>
                            <td>Рисуване</td>
                            <td>Физическо възпитание</td>
                          </tr>
                          <tr>
                            <th>Вторник</th>
                            <td>Математика</td>
                            <td>Математика</td>
                            <td>Четене</td>
                            <td>Четене</td>
                            <td>Домашен бит и техника</td>
                            <td>Домашен бит и техника</td>
                          </tr>
                          <tr>
                            <th>Сряда</th>
                            <td>Околен свят</td>
                            <td>Околен свят</td>
                            <td>БЕЛ</td>
                            <td>БЕЛ</td>
                            <td>Физическо възпитание</td>
                            <td>Физическо възпитание</td>
                          </tr>
                          <tr>
                            <th>Четвъртък</th>
                            <td>Математика</td>
                            <td>Математика</td>
                            <td>Английски език</td>
                            <td>Английски език</td>
                            <td>Рисуване</td>
                            <td>Рисуване</td>
                          </tr>
                        <tr>
                            <th>Петък</th>
                            <td>Четене</td>
                            <td>Четене</td>
                            <td>Информационни технологии</td>
                            <td>Информационни технологии</td>
                            <td>Рисуване</td>
                            <td>Домашен бит и техника</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection