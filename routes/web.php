<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

//Edit Profile
Route::get('/editprofile', 'EditProfileController@index');
Route::post('/edit_profile/update',[
    'as' => 'addentry', 'uses' => 'EditProfileController@update']);		//update profile info

//Curriculum
Route::get('/curriculum', 'CurriculumController@index');

