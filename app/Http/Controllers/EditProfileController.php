<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\User;
use Auth;

class EditProfileController extends Controller
{
    public function index()
    {
        $entries = User::where('id', '=', Auth::id())->get();
        return view('auth/editprofile', compact('entries'));
    }

    public function update() {
        $entry = User::find(Auth::id());
        $entry->first_name = Input::get('first_name');
        $entry->last_name = Input::get('last_name');
        $entry->email = Input::get('email');
        $entry->user_role = Input::get('user_role');
        $entry->save();

        return redirect('editprofile');
    }
}